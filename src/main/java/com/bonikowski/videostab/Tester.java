package com.bonikowski.videostab;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.Video;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

import java.io.File;

public class Tester {

    static Mat[] getTransforms(Mat input, Mat output) {
        MatOfPoint tmpPts = new MatOfPoint();
        MatOfPoint2f inputPts = new MatOfPoint2f();
        MatOfPoint2f outputPts = new MatOfPoint2f();
        MatOfByte status = new MatOfByte();
        MatOfFloat err = new MatOfFloat();

        Imgproc.goodFeaturesToTrack(input, tmpPts, 200, 0.01, 30);
        tmpPts.convertTo(inputPts, CvType.CV_32F);
        Video.calcOpticalFlowPyrLK(input, output, inputPts, outputPts, status, err);

        Mat homogpraphy = Calib3d.findHomography(inputPts, outputPts);
        Mat affine = Calib3d.estimateAffine2D(inputPts, outputPts);

        return new Mat[]{homogpraphy, affine};
    }

    static double[] getMetrics(File inputFile, File outputFile) {
        VideoCapture inputCap = new VideoCapture(inputFile.getPath());
        VideoCapture outputCap = new VideoCapture(outputFile.getPath());
        int frameCount = (int) Math.min(inputCap.get(Videoio.CAP_PROP_FRAME_COUNT), outputCap.get(Videoio.CAP_PROP_FRAME_COUNT));

        Mat inputFrame = new Mat();
        Mat outputFrame = new Mat();

        double cropping = 0, distortion = Float.MAX_VALUE, stability = Float.MAX_VALUE;

        for (int i = 1; i <= frameCount; i++) {
            inputCap.read(inputFrame);
            Imgproc.cvtColor(inputFrame, inputFrame, Imgproc.COLOR_BGR2GRAY);
            outputCap.read(outputFrame);
            Imgproc.cvtColor(outputFrame, outputFrame, Imgproc.COLOR_BGR2GRAY);

            Mat[] transforms = getTransforms(inputFrame, outputFrame);
            Mat homography = transforms[0];
            Mat affine = transforms[1];

            // cropping ratio: wymaga dekompozycji homografii, ale nie mam macierzy kalibracji
            // https://docs.opencv.org/3.0-beta/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html#decomposehomographymat

            // distortion
            Mat eigenvalues = new Mat();
            Mat homographySquare = homography.submat(0, 2, 0, 2);
            boolean eigenResult = Core.eigen(homographySquare, eigenvalues);
            if (!eigenResult) {
                System.out.println(String.format("Error while calculating eigenvalues (iteration %d/%d)", i, frameCount));
                break;
            }
            double tmpDistortion = Math.abs(eigenvalues.get(1, 0)[0] / eigenvalues.get(0, 0)[0]);
            if (tmpDistortion < distortion) distortion = tmpDistortion;

            // stability:
            //  The camera path is computed as accumulated Homography transformations between successive frames:

            System.out.print("\r" + String.format("Calculating metrics: %d / %d", i, frameCount));
        }

        return new double[]{Double.NaN, distortion, Double.NaN};
    }
}
