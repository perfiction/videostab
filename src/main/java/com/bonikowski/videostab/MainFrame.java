package com.bonikowski.videostab;

import org.opencv.core.Core;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.File;
import java.util.logging.Logger;

public class MainFrame {
    public static final Logger LOGGER = Logger.getLogger(MainFrame.class.getName());
    private DefaultTableModel model;
    private JLabel fileCountTextField;
    private JButton selectFilesButton;
    private JPanel mainPanel;
    private JButton stabilizeButton;
    private JTable resultsTable;
    private JProgressBar stabilizationProgressBar;
    private JLabel stabilizationProgressLabel;

    public MainFrame() {
        selectFilesButton.addActionListener(e -> {
            JFileChooser fc = new JFileChooser();
            fc.setMultiSelectionEnabled(true);
            int result = fc.showOpenDialog(null);
            if (result == JFileChooser.APPROVE_OPTION) {
                model.setRowCount(0);
                for (File file : fc.getSelectedFiles()) {
                    model.addRow(new Object[]{file.getPath(), " ", " ", " "});
                }
                fileCountTextField.setText("" + fc.getSelectedFiles().length);
            }
        });
        stabilizeButton.addActionListener(e -> {
            if (model.getRowCount() == 0) {
                JOptionPane.showMessageDialog(mainPanel, "Select at least one video first", "No videos selected", JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Stabilizer stab = new Stabilizer();
                    int filesCount = model.getRowCount();
                    stabilizationProgressBar.setValue(0);
                    stabilizationProgressBar.setMaximum(filesCount);
                    for (int i = 0; i < filesCount; i++) {
                        File videoFile = new File((String) model.getValueAt(i, 0));
                        stabilizationProgressLabel.setText(String.format("Video %d/%d: Analyzing...", i + 1, filesCount));
                        stab.analyzeVideo(videoFile);
                        stabilizationProgressLabel.setText(String.format("Video %d/%d: Stabilizing...", i + 1, filesCount));
                        stab.stabilizeVideo(videoFile);

                        String outputFileName = videoFile.getPath().replaceFirst("[.][^.]+$", "") + "_stabilized.mp4";
                        File outputFile = new File(outputFileName);
                        stabilizationProgressLabel.setText(String.format("Video %d/%d: Calculating metrics...", i + 1, filesCount));
                        double metrics[] = Tester.getMetrics(videoFile, outputFile);
                        model.setValueAt(String.format("%.4f", metrics[0]), i, 1);
                        model.setValueAt(String.format("%.4f", metrics[1]), i, 2);
                        model.setValueAt(String.format("%.4f", metrics[2]), i, 3);

                        stabilizationProgressBar.setValue(i + 1);
                    }
                    stabilizationProgressLabel.setText("Finished");
                }
            }).start();
        });
        mainPanel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                float[] columnWidthPercentage = {0.5f, 0.15f, 0.15f, 0.15f};
                for (int i = 0; i < model.getColumnCount(); i++) {
                    int colWidith = Math.round(columnWidthPercentage[i] * resultsTable.getWidth());
                    resultsTable.getColumnModel().getColumn(i).setPreferredWidth(colWidith);
                }
            }
        });
    }

    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            LOGGER.severe(e.toString());
        }

        JFrame frame = new JFrame();
        frame.setTitle("VideoStab");
        frame.setBounds(100, 100, 640, 480);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        frame.setContentPane(new MainFrame().mainPanel);
        frame.setVisible(true);
    }

    private void createUIComponents() {
        resultsTable = new JTable();
        model = new DefaultTableModel(0, 0);
        model.setColumnIdentifiers(new String[]{"File", "Cropping ratio", "Distortion value", "Stability score"});
        resultsTable.setModel(model);
    }
}
