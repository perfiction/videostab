package com.bonikowski.videostab.datatypes;

public class MoveInfo {
    public double x;
    public double y;
    public double angle;

    public MoveInfo(double x, double y, double angle) {
        this.x = x;
        this.y = y;
        this.angle = angle;
    }
}
