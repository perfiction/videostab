package com.bonikowski.videostab;

import org.apache.commons.math3.util.MathArrays;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.Video;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;
import org.opencv.videoio.Videoio;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Stabilizer {
    private static final int SMOOTHING_RADIUS = 50;

    private List<double[]> differences;

    void analyzeVideo(File video) {
        VideoCapture cap = new VideoCapture(video.getPath());
        int frameCount = (int) cap.get(Videoio.CAP_PROP_FRAME_COUNT);

        if (differences != null) differences.clear();
        else differences = new ArrayList<>();

        Mat prevFrame = new Mat();
        Mat frame = new Mat();

        MatOfPoint tmpKeypoints = new MatOfPoint();
        MatOfPoint2f prevKeypoints = new MatOfPoint2f();
        MatOfPoint2f keypoints = new MatOfPoint2f();

        cap.read(prevFrame);
        Imgproc.cvtColor(prevFrame, prevFrame, Imgproc.COLOR_BGR2GRAY);

        for (int i = 0; i < frameCount; i++) {
            boolean result = cap.read(frame);
            if (!result) break;

            Imgproc.goodFeaturesToTrack(prevFrame, tmpKeypoints, 200, 0.01, 30, new Mat(), 3);
            tmpKeypoints.convertTo(prevKeypoints, CvType.CV_32F);

            Imgproc.cvtColor(frame, frame, Imgproc.COLOR_BGR2GRAY);

            MatOfByte status = new MatOfByte();
            MatOfFloat error = new MatOfFloat();
            Video.calcOpticalFlowPyrLK(prevFrame, frame, prevKeypoints, keypoints, status, error);

            MatOfPoint2f[] validPoints = filterValidPoints(prevKeypoints, keypoints, status);
            MatOfPoint2f validPrevKeypoints = validPoints[0];
            MatOfPoint2f validKeypoints = validPoints[1];

            Mat transform = Calib3d.estimateAffinePartial2D(validPrevKeypoints, validKeypoints); // OpenCV > 3.0
//            Mat transform = Video.estimateRigidTransform(validPrevKeypoints, validKeypoints, false); // OpenCV < 3.0

            differences.add(getDifferenceFromTransform(transform));

            frame.copyTo(prevFrame);

            System.out.print("\r" + String.format("Analyzing: %d / %d (tracked points: %d)", i + 2, frameCount, validPrevKeypoints.rows()));
        }

        System.out.println();

        //<editor-fold desc="Free OpenCV objects">
        cap.release();
        prevFrame.release();
        frame.release();
        tmpKeypoints.release();
        prevKeypoints.release();
        keypoints.release();
        //</editor-fold>
    }

    void stabilizeVideo(File video) {
        List<double[]> trajectory = cumSum(differences);
        //List<double[]> smoothedTrajectory = getSmoothedTrajectory(trajectory);
        List<double[]> smoothedTrajectory = getSmoothedTrajectoryConvolution(trajectory);
        List<Mat> smoothedTransforms = getSmoothedTransforms(differences, trajectory, smoothedTrajectory);

        VideoCapture cap = new VideoCapture(video.getPath());
        int frameCount = (int) cap.get(Videoio.CAP_PROP_FRAME_COUNT);

        String newFileName = video.getName().replaceFirst("[.][^.]+$", "");
        String outputPath = video.getParent() + "/" + newFileName + "_stabilized.mp4";
        VideoWriter writer = new VideoWriter(outputPath,
                VideoWriter.fourcc('M', 'P', 'E', 'G'),
                cap.get(Videoio.CAP_PROP_FPS),
                new Size(cap.get(Videoio.CAP_PROP_FRAME_WIDTH), cap.get(Videoio.CAP_PROP_FRAME_HEIGHT)));

        Mat stableFrame = new Mat();
        Mat frame = new Mat();

        for (int i = 0; i < frameCount - 1; i++) {
            boolean result = cap.read(frame);
            if (!result) break;

            Mat transform = smoothedTransforms.get(i);
            Imgproc.warpAffine(frame, stableFrame, transform, frame.size());
            writer.write(stableFrame);

            System.out.print("\r" + String.format("Stabilizing: %d / %d", i + 2, frameCount));
        }

        System.out.println();
        writer.release();
        cap.release();
        frame.release();
        stableFrame.release();
    }

    public static List<double[]> getSmoothedTrajectoryConvolution(List<double[]> trajectory) {
        List<double[]> smoothed = new ArrayList<>();
        for (int i = 0; i < trajectory.size(); i++)
            smoothed.add(new double[]{0, 0, 0});

        for (int i = 0; i < 3; i++) {
            double[] component = new double[trajectory.size()];
            for (int j = 0; j < trajectory.size(); j++)
                component[j] = trajectory.get(j)[i];
            double[] smoothedComponent = getConvolution(component);
            for (int j = 0; j < smoothedComponent.length; j++)
                smoothed.get(j)[i] = smoothedComponent[j];
        }

        return smoothed;
    }

    public static double[] getConvolution(double[] arr) {
        int windowSize = 2 * SMOOTHING_RADIUS + 1;

        double[] kernel = new double[windowSize];
        for (int i = 0; i < windowSize; i++)
            kernel[i] = 1.0 / (double) windowSize;

        double[] tempTrajectory = new double[arr.length + (2 * SMOOTHING_RADIUS)];
        for (int j = 0; j < SMOOTHING_RADIUS; j++) {
            tempTrajectory[j] = arr[0];
            tempTrajectory[tempTrajectory.length - 1 - j] = arr[arr.length - 1];
        }

        for (int j = 0; j < arr.length; j++)
            tempTrajectory[j + SMOOTHING_RADIUS] = arr[j];

        double[] conv = MathArrays.convolve(tempTrajectory, kernel);
        double[] same = Arrays.copyOfRange(conv, 2 * SMOOTHING_RADIUS, conv.length - 2 * SMOOTHING_RADIUS);

        return same;
    }

    private MatOfPoint2f[] filterValidPoints(MatOfPoint2f prevPts, MatOfPoint2f currPts, MatOfByte status) {
        Point[] prevPtsArray = prevPts.toArray();
        Point[] currPtsArray = currPts.toArray();

        List<Point> validPrevPtsList = new ArrayList<>();
        List<Point> validCurrPtsList = new ArrayList<>();

        for (int i = 0; i < status.height(); i++) {
            if (status.get(i, 0)[0] == 1) {
                validPrevPtsList.add(new Point(prevPtsArray[i].x, prevPtsArray[i].y));
                validCurrPtsList.add(new Point(currPtsArray[i].x, currPtsArray[i].y));
            }
        }

        MatOfPoint2f validPrevPts = new MatOfPoint2f();
        validPrevPts.fromList(validPrevPtsList);

        MatOfPoint2f validCurrPts = new MatOfPoint2f();
        validCurrPts.fromList(validCurrPtsList);

        return new MatOfPoint2f[]{validPrevPts, validCurrPts};
    }

    double[] getDifferenceFromTransform(Mat transform) {
        double[] difference = {0, 0, 0};

        difference[0] = transform.get(0, 2)[0];
        difference[1] = transform.get(1, 2)[0];
        difference[2] = Math.atan2(-transform.get(1, 0)[0], transform.get(0, 0)[0]);

        return difference;
    }

    Mat getTransformFromDifference(double[] difference) {
        Mat transform = new Mat(2, 3, CvType.CV_32F);

        transform.put(0, 0, Math.cos(difference[2]));
        transform.put(0, 1, -Math.sin(difference[2]));
        transform.put(1, 0, Math.sin(difference[2]));
        transform.put(1, 1, Math.cos(difference[2]));

        transform.put(0, 2, difference[0]);
        transform.put(1, 2, difference[1]);

        return transform;
    }

    List<double[]> cumSum(List<double[]> differences) {
        List<double[]> trajectory = new ArrayList<>();
        double[] tmp = {0, 0, 0};

        for (double[] difference : differences) {
            tmp[0] += difference[0];
            tmp[1] += difference[1];
            tmp[2] += difference[2];

            trajectory.add(tmp.clone());
        }

        return trajectory;
    }

    List<double[]> getSmoothedTrajectory(List<double[]> trajectory) {
        List<double[]> smoothed = new ArrayList<>();

        for (int i = 0; i < trajectory.size(); i++) {
            double sumX = 0;
            double sumY = 0;
            double sumA = 0;
            int count = 0;

            for (int j = -SMOOTHING_RADIUS; j <= SMOOTHING_RADIUS; j++) {
                if (i + j >= 0 && i + j < trajectory.size()) {
                    sumX += trajectory.get(i + j)[0];
                    sumY += trajectory.get(i + j)[1];
                    sumA += trajectory.get(i + j)[2];
                    count++;
                }
            }
            if (count > 0)
                smoothed.add(new double[]{sumX / (double) count, sumY / (double) count, sumA / (double) count});
        }

        return smoothed;
    }

    List<Mat> getSmoothedTransforms(List<double[]> transforms, List<double[]> trajectory, List<double[]> smoothedTrajectory) {
        List<Mat> smoothedTransforms = new ArrayList<>();

        for (int i = 0; i < transforms.size(); i++) {
            double diffX = smoothedTrajectory.get(i)[0] - trajectory.get(i)[0];
            double diffY = smoothedTrajectory.get(i)[1] - trajectory.get(i)[1];
            double diffA = smoothedTrajectory.get(i)[2] - trajectory.get(i)[2];

            double dx = transforms.get(i)[0] + diffX;
            double dy = transforms.get(i)[1] + diffY;
            double da = transforms.get(i)[2] + diffA;

            smoothedTransforms.add(getTransformFromDifference(new double[]{dx, dy, da}));
        }

        return smoothedTransforms;
    }
}