import sys

import cv2
import numpy as np
import scipy.signal

import video_stabilizer


def get_transforms(input_cap, output_cap):
    input_pts = cv2.goodFeaturesToTrack(input_cap, maxCorners=200, qualityLevel=0.01, minDistance=30, blockSize=3)
    output_pts, _, _ = cv2.calcOpticalFlowPyrLK(input_cap, output_cap, input_pts, None)

    homography, _ = cv2.findHomography(input_pts, output_pts)
    affine, _ = cv2.estimateAffine2D(input_pts, output_pts)

    return [homography, affine]


def get_metrics(input_file, output_file):
    input_cap = cv2.VideoCapture(input_file)
    output_cap = cv2.VideoCapture(output_file)
    frame_count = int(min(input_cap.get(cv2.CAP_PROP_FRAME_COUNT), output_cap.get(cv2.CAP_PROP_FRAME_COUNT)))

    prev_output_frame = 0

    differences = []

    cropping = 0
    distortion = sys.float_info.max
    stability = sys.float_info.max

    for i in range(frame_count):
        input_result, input_frame = input_cap.read()
        input_frame = cv2.cvtColor(input_frame, cv2.COLOR_BGR2GRAY)
        if not input_result:
            break
        output_result, output_frame = output_cap.read()
        output_frame = cv2.cvtColor(output_frame, cv2.COLOR_BGR2GRAY)
        if not output_result:
            break

        [homography, affine] = get_transforms(input_frame, output_frame)

        # cropping ratio - skala wyciagnieta z affine matrix:
        # https://stackoverflow.com/questions/29510015/opencv-2-how-can-i-calculate-only-the-translation-and-scale-between-two-images
        scale_x = np.sign(affine[0, 0]) * np.sqrt(pow(affine[0, 0], 2) + pow(affine[0, 1], 2))
        scale_y = np.sign(affine[1, 1]) * np.sqrt(pow(affine[1, 0], 2) + pow(affine[1, 1], 2))
        cropping += scale_x * scale_y

        # distortion - stosunek dwoch najwiekszych wartosci wlasnych
        homography_square = homography[:2, :2]
        eigenvalues, _ = cv2.eigenNonSymmetric(homography_square)
        tmp_distortion = abs(eigenvalues[1,0] / eigenvalues[0,0])
        if tmp_distortion < distortion:
            distortion = tmp_distortion

        # stability - trzeba wyznaczyc trajektorie w stabilnym filmie
        if i > 0:
            prev_keypoints = cv2.goodFeaturesToTrack(prev_output_frame, maxCorners=200, qualityLevel=0.01, minDistance=30, blockSize=3)
            keypoints, status, _ = cv2.calcOpticalFlowPyrLK(prev_output_frame, output_frame, prev_keypoints, None)
            valid_prev_keypoints, valid_keypoints = video_stabilizer.filter_valid_points(prev_keypoints, keypoints, status)
            [transform, _] = cv2.estimateAffinePartial2D(valid_prev_keypoints, valid_keypoints)  # OpenCV 4.x and newer
            differences.append(video_stabilizer.get_difference_from_transform(transform))

        prev_output_frame = output_frame

        print("\rCalculating metrics: frame {} / {}".format(i + 1, frame_count), end='')

    # stability - po wyznaczeniu trajektorii sprawdzam wklad energii skladowych 2-6
    trajectory = np.cumsum(differences, axis=0)
    for i in range(3):
        signal = [column[i] for column in trajectory]
        [_, power] = scipy.signal.periodogram(signal)
        total_power = sum(power)
        low_frequencies_power = sum(power[1:6])
        if low_frequencies_power / total_power < stability:
            stability = low_frequencies_power / total_power

    cropping /= frame_count

    print()
    return cropping, distortion, stability
