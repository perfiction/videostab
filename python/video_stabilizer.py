import sys
import os
import time

import cv2
import numpy as np

import tester

SMOOTHING_RADIUS = 50


def filter_valid_points(prev_keypoints, keypoints, status):
    valid_indices = np.where(status == 1)[0]
    valid_prev_keypoints = prev_keypoints[valid_indices]
    valid_keypoints = keypoints[valid_indices]

    return valid_prev_keypoints, valid_keypoints


def get_difference_from_transform(transform):
    diff = [0, 0, 0]
    diff[0] = transform[0, 2]
    diff[1] = transform[1, 2]
    diff[2] = np.arctan2(transform[1, 0], transform[0, 0])

    return diff


def get_transform_from_difference(difference):
    transform = np.zeros((2, 3), np.float32)

    transform[0, 0] = np.cos(difference[2])
    transform[0, 1] = -np.sin(difference[2])
    transform[1, 0] = np.sin(difference[2])
    transform[1, 1] = np.cos(difference[2])
    transform[0, 2] = difference[0]
    transform[1, 2] = difference[1]

    return transform


def get_smoothed_trajectory_convolution(trajectory):
    smoothed = np.copy(trajectory)
    for i in range(3):
        smoothed[:, i] = get_convolution(trajectory[:, i])

    return smoothed


def get_convolution(component):
    window_size = 2 * SMOOTHING_RADIUS + 1
    kernel = np.ones(window_size) / window_size

    component_padded = np.lib.pad(component, (SMOOTHING_RADIUS, SMOOTHING_RADIUS), 'edge')
    component_smoothed = np.convolve(component_padded, kernel, mode='same')
    component_smoothed = component_smoothed[SMOOTHING_RADIUS: -SMOOTHING_RADIUS]

    return component_smoothed


def get_smoothed_transforms(differences, trajectory, smoothed_trajectory):
    smoothed_transforms = []
    smoothed_differences = differences + (smoothed_trajectory - trajectory)
    for diff in smoothed_differences:
        smoothed_transforms.append(get_transform_from_difference(diff))

    return smoothed_transforms


def analyze_video(video_path):
    cap = cv2.VideoCapture(video_path)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    differences = []

    _, prev_frame = cap.read()
    prev_frame = cv2.cvtColor(prev_frame, cv2.COLOR_BGR2GRAY)

    for i in range(frame_count):
        result, frame = cap.read()
        if not result:
            break
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        prev_keypoints = cv2.goodFeaturesToTrack(prev_frame, maxCorners=200, qualityLevel=0.01, minDistance=30, blockSize=3)

        keypoints, status, _ = cv2.calcOpticalFlowPyrLK(prev_frame, frame, prev_keypoints, None)
        valid_prev_keypoints, valid_keypoints = filter_valid_points(prev_keypoints, keypoints, status)

        [transform, _] = cv2.estimateAffinePartial2D(valid_prev_keypoints, valid_keypoints)  # OpenCV 4.x and newer

        differences.append(get_difference_from_transform(transform))

        prev_frame = frame

        print("\rAnalyzing: frame {} / {} (tracked points: {})".format(i + 2, frame_count, len(valid_prev_keypoints)), end='')

    print()
    return differences


def stabilize_video(video_path, differences):
    trajectory = np.cumsum(differences, axis=0)
    smoothed_trajectory = get_smoothed_trajectory_convolution(trajectory)
    smoothed_transforms = get_smoothed_transforms(differences, trajectory, smoothed_trajectory)

    cap = cv2.VideoCapture(video_path)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    parent_directory = os.path.dirname(video_path)
    [new_file_name, extension] = os.path.splitext(os.path.basename(video_path))
    output_path = parent_directory + "\\" + new_file_name + "_stabilized" + extension

    writer = cv2.VideoWriter(output_path,
                             cv2.VideoWriter_fourcc(*'MPEG'),
                             cap.get(cv2.CAP_PROP_FPS),
                             (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))))

    for i in range(frame_count - 1):
        result, frame = cap.read()
        if not result or i >= len(smoothed_transforms):
            break

        transform = smoothed_transforms[i]
        stable_frame = cv2.warpAffine(frame, transform, (frame.shape[1], frame.shape[0]))
        writer.write(stable_frame)

        print("\rStabilizing: frame {} / {}".format(i + 2, frame_count), end='')

    cap.release()
    writer.release()

    print()
    return output_path


def main(argv):
    if len(argv) == 0 or (len(argv) == 1 and argv[0] not in ("-s", "--stabilize", "-t", "--test")):
        print("Usage:\n\n"
              "Stabilization:\n"
              "video_stabilizer.py -s <video_1> <video_2> ... <video_n>\n"
              "video_stabilizer.py --stabilize <video_1> <video_2> ... <video_n>\n\n"
              "Calculate metrics:\n"
              "video_stabilizer.py -t <video> <stabilized_video>\n"
              "video_stabilizer.py --test <video> <stabilized_video>\n")
    elif argv[0] in ("-s", "--stabilize"):
        if len(argv) == 1:
            print("Usage:\n"
                  "video_stabilizer.py -s <video_1> <video_2> ... <video_n>\n"
                  "video_stabilizer.py --stabilize <video_1> <video_2> ... <video_n>")
        else:
            for video_file in argv[1:]:
                if not os.path.exists(video_file) or not os.path.isfile(video_file):
                    print("File {} not found".format(video_file))
                    continue

                start = time.time()
                differences = analyze_video(video_file)
                output_file = stabilize_video(video_file, differences)
                stop = time.time()
                print("Stabilization finished after {} seconds".format(stop - start))

                [cropping, distortion, stability] = tester.get_metrics(video_file, output_file)
                print("Cropping ratio: {}\nDistortion score: {}\nStability score: {}".format(cropping, distortion, stability))
    elif argv[0] in ("-t", "--test"):
        if len(argv) != 3:
            print("Usage:\n"
                  "video_stabilizer.py -t <video> <stabilized_video>\n"
                  "video_stabilizer.py --test <video> <stabilized_video>")
        else:
            if not os.path.exists(argv[1]) or not os.path.isfile(argv[1]):
                print("File {} not found".format(argv[1]))
                sys.exit()
            if not os.path.exists(argv[1]) or not os.path.isfile(argv[1]):
                print("File {} not found".format(argv[1]))
                sys.exit()

            [cropping, distortion, stability] = tester.get_metrics(argv[1], argv[2])
            print("Cropping ratio: {}\nDistortion score: {}\nStability score: {}".format(cropping, distortion, stability))


if __name__ == "__main__":
    main(sys.argv[1:])
